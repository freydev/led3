var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('watch', function() {
    gulp.watch('./main/static/less/*.less', ['buildLess'])
});

gulp.task('buildLess', function () {
    gulp.src('./main/static/less/style.less')
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.init())
        .pipe(minifyCss())
        .pipe(sourcemaps.write('../../maps'))
        .pipe(gulp.dest('./main/static/css'));
});

gulp.task('default', ['watch', 'buildLess']);