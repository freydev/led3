#! coding: UTF-8
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import permalink
from django_ymap.fields import YmapCoord
from redactor.fields import RedactorField


class Order(models.Model):
    company = models.CharField(max_length=255, verbose_name=u'Компания')
    person = models.CharField(max_length=255, verbose_name=u'Контактное лицо')
    email = models.EmailField(max_length=255, verbose_name=u'Email')
    phone = models.CharField(max_length=255, verbose_name=u'Телефон')
    comment = models.TextField()

    leds = models.ManyToManyField('leds.Led', verbose_name=u'Экраны в заказе')
    groups = models.ManyToManyField('leds.LedGroup', verbose_name=u'Пакеты в заказе')

    success = models.BooleanField(default=False, verbose_name=u'Выполнен')

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'

    def get_admin_link(self):
        return reverse('admin:main_order_change', args=(self.id,))

    def __unicode__(self):
        return '%s, %s' %(self.pk, self.person)


class Question(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Имя')
    phone = models.CharField(max_length=255, verbose_name=u'Телефон')
    email = models.EmailField(max_length=255, verbose_name=u'Email')
    issue = models.TextField(verbose_name=u'Вопрос')
    success = models.BooleanField(default=False, verbose_name=u'Решен')

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'

    def __unicode__(self):
        return '%s, %s' %(self.name, self.issue)

    def get_admin_link(self):
        return reverse('admin:main_question_change', args=(self.id,))


class FastBid(models.Model):
    text = models.TextField(verbose_name=u'Быстрая заявка')
    success = models.BooleanField(default=False, verbose_name=u'Обработана')

    class Meta:
        verbose_name = u'Быструю заявку'
        verbose_name_plural = u'Быстрые заявки'

    def __unicode__(self):
        return self.text

    def get_admin_link(self):
        return reverse('admin:main_fastbid_change', args=(self.id,))

class LedoutdoorSettings(models.Model):
    index = RedactorField(verbose_name=u'Соержимое главной страницы', blank=True)
    about_led = RedactorField(verbose_name=u'Содержимое страницы "О LED экранах"', blank=True)
    contacts = RedactorField(verbose_name=u'Содержимое страницы "Контакты"', help_text=u'Содержание верхнего блока на странице "Контакты"', blank=True)
    phone_n = models.CharField(max_length=255, verbose_name=u'Телефон', help_text=u'Отображается в верхнем углу сайта на главной странице', blank=True)
    email_n = models.CharField(max_length=255, verbose_name=u'Почта', help_text=u'Отображается в верхнем углу сайта на главной странице', blank=True)
    email_for_notice = models.CharField(max_length=255, verbose_name=u'Почта для связи', help_text=u'На эту почту будут отсылаться уведомление о заказах', blank=True)
    address_coord = YmapCoord(max_length=200, default=None, blank=True, null=True, start_query=u'Москва', size_width=980,
                       size_height=500, verbose_name=u'Адрес офиса')

    title_index = models.CharField(max_length=255, verbose_name=u'Заголовок главной страницы', blank=True)
    description_index = models.CharField(max_length=255, verbose_name=u'Описание главной страницы', blank=True)
    keywords_index = models.CharField(max_length=255, verbose_name=u'Ключевые слова главной страницы', blank=True)

    title_cities = models.CharField(max_length=255, verbose_name=u'Заголовок страницы каталога', blank=True)
    description_cities = models.CharField(max_length=255, verbose_name=u'Описание страницы каталога', blank=True)
    keywords_cities = models.CharField(max_length=255, verbose_name=u'Ключевые слова страницы каталога', blank=True)

    title_about_led = models.CharField(max_length=255, verbose_name=u'Заголовок страницы о LED экранах', blank=True)
    description_about_led = models.CharField(max_length=255, verbose_name=u'Описание страницы о LED экранах', blank=True)
    keywords_about_led = models.CharField(max_length=255, verbose_name=u'Ключевые слова страницы о LED экранах', blank=True)

    title_city = models.CharField(max_length=255, verbose_name=u'Заголовок страницы города', blank=True, help_text=u'''
    Эти поля принимают шаблон, доступные переменные {{slug}}, {{description}}, {{name}}, {{center}}''')
    description_city = models.CharField(max_length=255, verbose_name=u'Описание страницы города', blank=True)
    keywords_city = models.CharField(max_length=255, verbose_name=u'Ключевые слова страницы города', blank=True)

    title_contact = models.CharField(max_length=255, verbose_name=u'Заголовок страницы контактов', blank=True)
    description_contact = models.CharField(max_length=255, verbose_name=u'Описание страницы контактов', blank=True)
    keywords_contact = models.CharField(max_length=255, verbose_name=u'Ключевые слова страницы контактов', blank=True)

    footer = RedactorField(verbose_name=u'Подвал', help_text=u'Отображается оостветственно в подвале сайта')
    snippets = models.TextField(blank=True, verbose_name=u'Код', help_text=u'JS код для счетчиков и прочего, вставляется в конец сайта')

    advantages_1 = models.TextField(blank=True)
    advantages_2 = models.TextField(blank=True)
    advantages_3 = models.TextField(blank=True)

    class Meta:
        verbose_name = u'Настройки сайта'
        verbose_name_plural = u'Настройки сайта'

    def __unicode__(self):
        return u'Настройки'
