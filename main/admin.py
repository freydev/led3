#! coding: UTF-8
from django.contrib import admin
from django_ymap.admin import YmapAdmin
from main.models import Order, Question, FastBid, LedoutdoorSettings


class OrderAdmin(admin.ModelAdmin):
    list_display = ('company', 'person', 'email', 'phone', 'success')
    filter_horizontal = ['leds', 'groups']


class FastBidAdmin(admin.ModelAdmin):
    list_display = ('text', 'success')


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email', 'success')


class SettingsAdmin(YmapAdmin, admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    fieldsets = (
        (None, {
            'fields': ('phone_n', 'email_n', 'email_for_notice', 'address_coord')
        }),
        (u'Содержание главной страницы', {
            'classes': ('collapse',),
            'fields': ('index',),
        }),
        (u'SEO Главная страница', {
            'classes': ('collapse',),
            'fields': ('title_index', 'description_index', 'keywords_index')
        }),

        (u'Содержание страницы о LED экранах', {
            'classes': ('collapse',),
            'fields': ('about_led',),
        }),
        (u'SEO Страница o LED экранах', {
            'classes': ('collapse',),
            'fields': ('title_about_led', 'description_about_led', 'keywords_about_led')
        }),

        (u'SEO Страница списка городов', {
            'classes': ('collapse',),
            'fields': ('title_cities', 'description_cities', 'keywords_cities')
        }),

        (u'SEO Страница города', {
            'classes': ('collapse',),
            'fields': ('title_city', 'description_city', 'keywords_city')
        }),

        (u'Содержание страницы контактов', {
            'classes': ('collapse',),
            'fields': ('contacts',),
        }),

        (u'SEO Странца контактов', {
            'classes': ('collapse',),
            'fields': ('title_contact', 'description_contact', 'keywords_contact')
        }),
        (u'Подвал', {
            'classes': ('collapse',),
            'fields': ('footer',),
        }),

        (u'Приемущестава', {
            'classes': ('advantages',),
            'fields': ('advantages_1', 'advantages_2', 'advantages_3')
        }),

        (u'Счетчики', {
            'classes': ('collapse',),
            'fields': ('snippets',)
        })
    )


admin.site.register(Order, OrderAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(FastBid, FastBidAdmin)
admin.site.register(LedoutdoorSettings, SettingsAdmin)
