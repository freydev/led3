from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from cities.views import CityView
from leds.views import JsonContent

urlpatterns = [
                  url(r'^admin/', include(admin.site.urls)),

                  url(r'^$', TemplateView.as_view(template_name='index.html'), name='home'),
                  url(r'^cities$', TemplateView.as_view(template_name='cities.html'), name='cities'),

                  url(r'^cities/(?P<slug>.*)$', CityView.as_view(), name='city'),
                  url(r'^about/', TemplateView.as_view(template_name='about.html'), name='about_led'),
                  url(r'^contacts/', TemplateView.as_view(template_name='contacts.html'), name='contacts'),
                  url(r'^api/', csrf_exempt(JsonContent.as_view()), name='api'),

                  url(r'^redactor/', include('redactor.urls'))
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
