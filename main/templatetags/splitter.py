from itertools import izip_longest
from django.template import Library
import math

register = Library()

@register.filter
def splitter(iterable, n):
    def grouper(iterable, n):
        args = [iter(iterable)] * n
        return izip_longest(*args)

    length = int(math.ceil(len(iterable) / float(n)))
    return grouper(iterable, length)