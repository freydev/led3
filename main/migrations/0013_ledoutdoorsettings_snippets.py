# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20151013_1302'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='snippets',
            field=models.TextField(default='', verbose_name='JS \u043a\u043e\u0434 \u0434\u043b\u044f \u0441\u0447\u0435\u0442\u0447\u0438\u043a\u043e\u0432 \u0438 \u043f\u0440\u043e\u0447\u0435\u0433\u043e, \u0432\u0441\u0442\u0430\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u0432 \u043a\u043e\u043d\u0435\u0446 \u0441\u0430\u0439\u0442\u0430'),
            preserve_default=False,
        ),
    ]
