# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20151011_1852'),
    ]

    operations = [
        migrations.CreateModel(
            name='FastBid',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='\u0411\u044b\u0441\u0442\u0440\u0430\u044f \u0437\u0430\u044f\u0432\u043a\u0430')),
            ],
            options={
                'verbose_name': '\u0411\u044b\u0441\u0442\u0440\u0443\u044e \u0437\u0430\u044f\u0432\u043a\u0443',
                'verbose_name_plural': '\u0411\u044b\u0441\u0442\u0440\u044b\u0435 \u0437\u0430\u044f\u0432\u043a\u0443',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('phone', models.CharField(max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email', models.EmailField(max_length=255, verbose_name='Email')),
                ('issue', models.TextField(verbose_name='\u0412\u043e\u043f\u0440\u043e\u0441')),
            ],
            options={
                'verbose_name': '\u0412\u043e\u043f\u0440\u043e\u0441',
                'verbose_name_plural': '\u0412\u043e\u043f\u0440\u043e\u0441\u044b',
            },
        ),
    ]
