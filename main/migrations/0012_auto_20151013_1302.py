# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_auto_20151012_1213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='title_city',
            field=models.CharField(help_text='\n    \u042d\u0442\u0438 \u043f\u043e\u043b\u044f \u043f\u0440\u0438\u043d\u0438\u043c\u0430\u044e\u0442 \u0448\u0430\u0431\u043b\u043e\u043d, \u0434\u043e\u0441\u0442\u0443\u043f\u043d\u044b\u0435 \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u0435 {{slug}}, {{description}}, {{name}}, {{center}}', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0433\u043e\u0440\u043e\u0434\u0430', blank=True),
        ),
    ]
