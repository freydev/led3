# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_fastbid_question'),
    ]

    operations = [
        migrations.CreateModel(
            name='LedoutdoorSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('index', redactor.fields.RedactorField(verbose_name='\u0421\u043e\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b')),
                ('about_led', redactor.fields.RedactorField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b "\u041e LED \u044d\u043a\u0440\u0430\u043d\u0430\u0445"')),
                ('contacts', redactor.fields.RedactorField(help_text='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435 \u0432\u0435\u0440\u0445\u043d\u0435\u0433\u043e \u0431\u043b\u043e\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435 "\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b"', verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b "\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b"')),
                ('address', models.CharField(help_text='\u0414\u043b\u044f \u043a\u0430\u0440\u0442\u044b \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435 "\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b"', max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('phone_n', models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0432 \u0432\u0435\u0440\u0445\u043d\u0435\u043c \u0443\u0433\u043b\u0443 \u0441\u0430\u0439\u0442\u0430 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435', max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email_n', models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0432 \u0432\u0435\u0440\u0445\u043d\u0435\u043c \u0443\u0433\u043b\u0443 \u0441\u0430\u0439\u0442\u0430 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435', max_length=255, verbose_name='\u041f\u043e\u0447\u0442\u0430')),
                ('email_for_notice', models.CharField(help_text='\u041d\u0430 \u044d\u0442\u0443 \u043f\u043e\u0447\u0442\u0443 \u0431\u0443\u0434\u0443\u0442 \u043e\u0442\u0441\u044b\u043b\u0430\u0442\u044c\u0441\u044f \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u0435 \u043e \u0437\u0430\u043a\u0430\u0437\u0430\u0445', max_length=255, verbose_name='\u041f\u043e\u0447\u0442\u0430 \u0434\u043b\u044f \u0441\u0432\u044f\u0437\u0438')),
            ],
            options={
                'verbose_name': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0441\u0430\u0439\u0442\u0430',
            },
        ),
        migrations.AlterModelOptions(
            name='fastbid',
            options={'verbose_name': '\u0411\u044b\u0441\u0442\u0440\u0443\u044e \u0437\u0430\u044f\u0432\u043a\u0443', 'verbose_name_plural': '\u0411\u044b\u0441\u0442\u0440\u044b\u0435 \u0437\u0430\u044f\u0432\u043a\u0438'},
        ),
    ]
