# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20151012_0948'),
    ]

    operations = [
        migrations.AddField(
            model_name='fastbid',
            name='success',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u0430\u043d\u0430'),
        ),
        migrations.AddField(
            model_name='question',
            name='success',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0435\u0448\u0435\u043d'),
        ),
    ]
