# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('leds', '0008_auto_20151011_1831'),
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='groups',
            field=models.ManyToManyField(to='leds.LedGroup', verbose_name='\u041f\u0430\u043a\u0435\u0442\u044b \u0432 \u0437\u0430\u043a\u0430\u0437\u0435'),
        ),
        migrations.AddField(
            model_name='order',
            name='leds',
            field=models.ManyToManyField(to='leds.Led', verbose_name='\u042d\u043a\u0440\u0430\u043d\u044b \u0432 \u0437\u0430\u043a\u0430\u0437\u0435'),
        ),
    ]
