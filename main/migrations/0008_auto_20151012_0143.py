# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20151012_0118'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='advantages_1',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='advantages_2',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='advantages_3',
            field=models.TextField(blank=True),
        ),
    ]
