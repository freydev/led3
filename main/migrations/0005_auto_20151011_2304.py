# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20151011_2018'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='description_cities',
            field=models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u0430\u0442\u0430\u043b\u043e\u0433\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='description_city',
            field=models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0433\u043e\u0440\u043e\u0434\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='description_contact',
            field=models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u043e\u0432', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='description_index',
            field=models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='keywords_cities',
            field=models.CharField(max_length=255, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u0430\u0442\u0430\u043b\u043e\u0433\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='keywords_city',
            field=models.CharField(max_length=255, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0433\u043e\u0440\u043e\u0434\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='keywords_contact',
            field=models.CharField(max_length=255, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u043e\u0432', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='keywords_index',
            field=models.CharField(max_length=255, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='title_cities',
            field=models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u0430\u0442\u0430\u043b\u043e\u0433\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='title_city',
            field=models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0433\u043e\u0440\u043e\u0434\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='title_contact',
            field=models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u043e\u0432', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='title_index',
            field=models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='about_led',
            field=redactor.fields.RedactorField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b "\u041e LED \u044d\u043a\u0440\u0430\u043d\u0430\u0445"', blank=True),
        ),
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='address',
            field=models.CharField(help_text='\u0414\u043b\u044f \u043a\u0430\u0440\u0442\u044b \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435 "\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b"', max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='contacts',
            field=redactor.fields.RedactorField(help_text='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435 \u0432\u0435\u0440\u0445\u043d\u0435\u0433\u043e \u0431\u043b\u043e\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435 "\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b"', verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b "\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b"', blank=True),
        ),
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='email_for_notice',
            field=models.CharField(help_text='\u041d\u0430 \u044d\u0442\u0443 \u043f\u043e\u0447\u0442\u0443 \u0431\u0443\u0434\u0443\u0442 \u043e\u0442\u0441\u044b\u043b\u0430\u0442\u044c\u0441\u044f \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u0435 \u043e \u0437\u0430\u043a\u0430\u0437\u0430\u0445', max_length=255, verbose_name='\u041f\u043e\u0447\u0442\u0430 \u0434\u043b\u044f \u0441\u0432\u044f\u0437\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='email_n',
            field=models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0432 \u0432\u0435\u0440\u0445\u043d\u0435\u043c \u0443\u0433\u043b\u0443 \u0441\u0430\u0439\u0442\u0430 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435', max_length=255, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='index',
            field=redactor.fields.RedactorField(verbose_name='\u0421\u043e\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='ledoutdoorsettings',
            name='phone_n',
            field=models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0432 \u0432\u0435\u0440\u0445\u043d\u0435\u043c \u0443\u0433\u043b\u0443 \u0441\u0430\u0439\u0442\u0430 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435', max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
        ),
    ]
