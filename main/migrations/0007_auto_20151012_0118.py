# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20151011_2317'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ledoutdoorsettings',
            options={'verbose_name': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0441\u0430\u0439\u0442\u0430', 'verbose_name_plural': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0441\u0430\u0439\u0442\u0430'},
        ),
        migrations.AddField(
            model_name='order',
            name='success',
            field=models.BooleanField(default=False),
        ),
    ]
