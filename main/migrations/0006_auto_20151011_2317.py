# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20151011_2304'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='description_about_led',
            field=models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043e LED \u044d\u043a\u0440\u0430\u043d\u0430\u0445', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='keywords_about_led',
            field=models.CharField(max_length=255, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043e LED \u044d\u043a\u0440\u0430\u043d\u0430\u0445', blank=True),
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='title_about_led',
            field=models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043e LED \u044d\u043a\u0440\u0430\u043d\u0430\u0445', blank=True),
        ),
    ]
