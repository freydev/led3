# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20151012_0143'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='footer',
            field=redactor.fields.RedactorField(default='', help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u043e\u043e\u0441\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0435\u043d\u043d\u043e \u0432 \u043f\u043e\u0434\u0432\u0430\u043b\u0435 \u0441\u0430\u0439\u0442\u0430', verbose_name='\u041f\u043e\u0434\u0432\u0430\u043b'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='success',
            field=models.BooleanField(default=False, verbose_name='\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d'),
        ),
    ]
