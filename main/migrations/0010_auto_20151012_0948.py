# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_ymap.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20151012_0942'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ledoutdoorsettings',
            name='address',
        ),
        migrations.AddField(
            model_name='ledoutdoorsettings',
            name='address_coord',
            field=django_ymap.fields.YmapCoord(default=None, size_width=980, max_length=200, blank=True, address_field=b'', size_height=500, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u043e\u0444\u0438\u0441\u0430', start_query='\u041c\u043e\u0441\u043a\u0432\u0430'),
        ),
    ]
