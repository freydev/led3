from main.models import LedoutdoorSettings


def get_settings(request):
    settings = LedoutdoorSettings.objects.all()[0]
    return {
        'index_content': settings.index,
        'phone_number': settings.phone_n,
        'email': settings.email_n,
        'email_for_notice': settings.email_for_notice,
        'contacts_content': settings.contacts,
        'advantages_1': settings.advantages_1,
        'advantages_2': settings.advantages_2,
        'advantages_3': settings.advantages_3,
        'about_led_content': settings.about_led,
        'address_coord': settings.address_coord,
        'footer_content': settings.footer,

        'index_title': settings.title_index,
        'index_descripion': settings.description_index,
        'index_keywords': settings.keywords_index,

        'cities_title': settings.title_cities,
        'cities_descripion': settings.description_cities,
        'cities_keywords': settings.keywords_cities,

        'about_led_title': settings.title_about_led,
        'about_led_descripion': settings.description_about_led,
        'about_led_keywords': settings.keywords_about_led,

        'contacts_title': settings.title_contact,
        'contacts_descripion': settings.description_contact,
        'contacts_keywords': settings.keywords_contact,

        'snippets': settings.snippets
    }