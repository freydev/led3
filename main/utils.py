#coding: utf-8
from django.core.mail import send_mail

from django.http import HttpResponseBadRequest
from django.template import Template, Context
from main.models import LedoutdoorSettings
from main.site_settings import get_settings


def ajax_required(f):
    def wrap(request, *args, **kwargs):
            if not request.is_ajax():
                return HttpResponseBadRequest()
            return f(request, *args, **kwargs)
    wrap.__doc__=f.__doc__
    wrap.__name__=f.__name__
    return wrap


def sendmail(mess_type, obj):
    html = Template(u"""
        С сайта led-outdoor.ru пришел(а) новый(я) {{ mess_type }}
        ссылка: <a href="http://led-outdoor.ru{{ obj.get_admin_link }}">http://led-outdoor.ru{{ obj.get_admin_link }}</a>
    """).render(Context({
        'mess_type': mess_type,
        'obj': obj
    }))

    to = LedoutdoorSettings.objects.all()[0].email_for_notice
    return send_mail(u'Событие на сайте ledoutdoor.ru', '', 'noreply@ledoutdoor.ru', [to], html_message=html)

"""
The md5 and sha modules are deprecated since Python 2.5, replaced by the
hashlib module containing both hash algorithms. Here, we provide a common
interface to the md5 and sha constructors, depending on system version.
"""

import warnings
warnings.warn("django.utils.hashcompat is deprecated; use hashlib instead",
              PendingDeprecationWarning)

import hashlib
md5_constructor = hashlib.md5
md5_hmac = md5_constructor
sha_constructor = hashlib.sha1
sha_hmac = sha_constructor