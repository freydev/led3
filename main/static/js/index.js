var app = angular.module('App', ['angular.filter', 'yaMap', 'ngAnimate', 'ngDialog'])
    .config(function ($httpProvider, $interpolateProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');

    });

app.run(function ($rootScope, $timeout, ngDialog) {
    $rootScope.preventProcessLed = false;
    $rootScope.preventProcessGroup = false;
    $rootScope.start = true;

    $rootScope.leds = {};
    $rootScope.ledsGroup = {};
    $rootScope.removeLed = function (id) {
        $rootScope.leds = _.omit($rootScope.leds, id);
    };

    $rootScope.removeGroup = function (id) {
        $rootScope.ledsGroup = _.omit($rootScope.ledsGroup, id);
    };

    $rootScope.findLedInOrder = function (id) {
        return _.findKey($rootScope.leds, function (v, key) {
                return key == id
            }) > -1
    };

    $rootScope.findGroupInOrder = function (id) {
        return _.findKey($rootScope.ledsGroup, function (v, key) {
                return key == id
            }) > -1
    };

    $rootScope.leds_not_in_group = function () {
        return document.querySelector('.item_led') != null
    };

    $rootScope.has_group = function () {
        return _.keys($rootScope.ledsGroup).length > 0
    };

    $rootScope.in_order_count = function () {
        return _.keys($rootScope.leds).length
    };

    $rootScope.openOrder = function () {
        ngDialog.open({
            templateUrl: '/api?method=get_order&ids=' + _.keys($rootScope.leds).join(','),
            showClose: false,
            controller: 'orderController'
        })
    };

    $rootScope.openFastbid = function() {
        ngDialog.open({
            templateUrl: '/api?method=get_fastbid',
            showClose: false,
            controller: 'fastBidController'
        })
    };

    $rootScope.openDetail = function (id) {
        ngDialog.open({
            templateUrl: '/api?method=get_display&id=' + id,
            showClose: false,
            controller: 'detailController'
        });
    };


    $rootScope.$watchCollection('leds', function (newValue) {
        if ($rootScope.preventProcessLed) return;
        $rootScope.preventProcessGroup = true;
        $rootScope.preventProcessLed = true;

        // экраны сохраняются вида {1: true}, pick удаляет записи типа {1: false}
        $rootScope.leds = _.pick(newValue, function (v) {
            return v
        });

        // проверяем соответствуют ли группы выбранным экранам
        for (pack in $rootScope.ledsGroup) {
            var for_delete = false;
            $rootScope.ledsGroup[pack].forEach(function (led_id) {
                var _exist = _.findKey($rootScope.leds, function (v, key) {
                    return key == led_id
                });
                if (!_exist) for_delete = true
            });

            if (for_delete) delete $rootScope.ledsGroup[pack]
        }

        // ищет группы для добавления
        try {
            window.GROUPS.forEach(function (group) {
                var for_add = true;
                group.leds.forEach(function (id) {
                    var _exist = _.findKey($rootScope.leds, function (v, key) {
                        return key == id
                    });
                    if (!_exist) for_add = false;
                });

                if (for_add) $rootScope.ledsGroup[group.id] = group.leds;
            });
        } catch (e) {
            // на странице нет пакетов
        }

        $timeout(function () {
            $rootScope.preventProcessGroup = false;
            $rootScope.preventProcessLed = false;
            $rootScope.$digest();
            localStorage.setItem('in_order', JSON.stringify($rootScope.leds))
        }, 100)
    });

    $rootScope.$watchCollection('ledsGroup', function (newValue, oldValue) {
        if ($rootScope.preventProcessGroup) return;
        $rootScope.preventProcessLed = true;
        $rootScope.preventProcessGroup = true;
        $rootScope.ledsGroup = _.pick(newValue, function (v) {
            return v
        });

        var difference = _.omit(oldValue, _.keys($rootScope.ledsGroup));
        if (_.isEmpty(difference)) {
            for (var pack in $rootScope.ledsGroup) {
                // отмечаем экраны в группах
                $rootScope.ledsGroup[pack].forEach(function (led_id) {
                    $rootScope.leds[led_id] = true;
                })
            }
        } else {
            $rootScope.leds = _.omit($rootScope.leds, _.values(difference));
        }

        $timeout(function () {
            $rootScope.preventProcessLed = false;
            $rootScope.preventProcessGroup = false;
            $rootScope.start = false;
            $rootScope.$digest();
            localStorage.setItem('in_order', JSON.stringify($rootScope.leds))
        }, 100)
    });

    try {
        $rootScope.leds = JSON.parse(localStorage.getItem('in_order'));
        $rootScope.$digest();

        // соответствующие группы будут отмечены автоматически
    } catch (e) {
        // в хранилище нет информации о заказе
    }
});

app.controller('citiesController', function ($scope, $timeout, $interval) {
    $scope.cities = window.CITIES;
    $scope.balls = window.BALLS;
    $scope.contains = function (arr, str) {
        return _.find(arr, function (city) {
            return city.name.toLowerCase().indexOf(str.toLowerCase()) > -1
        })
    };

    var map_size = [923.0, 501.0];
    $scope.showRect = [0, 0];
    var showInterval;

    $timeout(function () {
        showInterval = $interval(function () {
            if ($scope.showRect[0] >= map_size[0]) {
                $interval.cancel(showInterval);
                showInterval = undefined
            } else {
                $scope.showRect[0] += 50;
                $scope.showRect = [$scope.showRect[0], $scope.showRect[0] / (map_size[0] / map_size[1])];
            }
        }, 100)
    }, 400)
});

app.controller('cityController', function ($scope, ngDialog, templateLayoutFactory, $rootScope, $timeout) {
    var map;
    $scope.afterMapInit = function (nMap) {
        map = nMap;
        $scope.mapLoaded = true;
    };

    $scope.centerOnCity = function (city, center) {
        if (center && center !== 'None') {
            $scope.center = center.split(',').reverse()
        } else {
            ymaps.geocode(city, {results: 1}).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                $scope.center = firstGeoObject.geometry.getCoordinates();
                $scope.$digest()
            }, function (err) {
                console.log(err.message);
            });
        }
    };

    $scope.updateProxy = function () {
        $timeout(function () {
            $scope.overrides.updateState();
        }, 10)
    };

    // YA-MAP
    $scope.openBalloon = function (e) {
        e.get('target').balloon.open()
    };

    // если добавили экран из попапа
    $rootScope.$watchCollection('leds', function () {
        $scope.overrides.updateState()
    });

    $scope.overrides = {
        updateState: function () {
            var checkbox = angular.element(document.getElementById('mapLedCheckbox'));
            if (checkbox.length)
                if ($rootScope.leds[(checkbox.attr('data-id'))])
                    checkbox.attr('checked', true);
                else checkbox.attr('checked', false);

            if (map)
                map.geoObjects.each(function (mark) {
                    if ($rootScope.leds[mark.options.get('led_id')])
                        mark.options.set('iconColor', 'red');
                    else
                        mark.options.set('iconColor', '#0496b0');


                    if (mark.options.get('led_id') == checkbox.attr('data-id'))
                        mark.options.set('iconColor', checkbox.attr('checked') ? 'red' : '#0496b0')
                });
        },

        build: function () {
            var BalloonContentLayout = templateLayoutFactory.get('balloon');
            BalloonContentLayout.superclass.build.call(this);
            angular.element(document.getElementById('mapInfoLink')).bind('click', this.onInfoClick);
            angular.element(document.getElementById('mapLedCheckbox'))
                .bind('change', this.onChangeOrder.bind(this));

            this.updateState();
        },

        clear: function () {
            angular.element(document.getElementById('mapInfoLink')).unbind('click', this.onInfoClick);
            var BalloonContentLayout = templateLayoutFactory.get('balloon');
            BalloonContentLayout.superclass.clear.call(this);
        },

        onInfoClick: function () {
            $scope.openDetail(document.getElementById('mapInfoLink').dataset.id)
        },

        onChangeOrder: function (ev) {
            var el = ev.target;
            switch (el.checked) {
                case true:
                    $rootScope.leds[el.dataset.id] = true;
                    break;
                case false:
                    $rootScope.leds[el.dataset.id] = false;
                    break;
            }
            $rootScope.$digest();
            $timeout(function () {
                this.updateState()
            }.bind(this), 100);
        }
    }
});

app.controller('detailController', function ($scope) {
    // dummy
});

app.controller('orderController', function ($scope, $http) {
    $scope.sendOrder = function () {
        $http.post('/api/?method=send_order', {
            company: $scope.customer.company,
            person: $scope.customer.person,
            email: $scope.customer.email,
            phone: $scope.customer.phone,
            comment: $scope.customer.comment,
            leds: _.keys(JSON.parse(localStorage.getItem('in_order')))
        });
        $scope.orderSended = true;
    };

});

app.controller('contactsController', function($scope, $http){
    $scope.sendIssue = function() {
        if (!$scope.customer.phone && !$scope.customer.email || !$scope.issueForm.$valid)
            return;
        $http.post('/api/?method=send_issue', {
            name: $scope.customer.name,
            phone: $scope.customer.phone,
            email: $scope.customer.email,
            issue: $scope.customer.issue
        });
        $scope.issueSended = true;
    }
});

app.controller('fastBidController', function($scope, $http) {
    $scope.sendBid = function() {
        $http.post('/api/?method=send_bid', {
            bid: $scope.customer.bid
        })
        $scope.bidSended = true;
    }
});

app.directive('tooltip', function ($animate, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (!attr['tooltip']) return;

            var template = angular.element('<div class="tooltip">' + attr['tooltip'] + '</div>'),
                $el = element[0],
                rect = element.find('label')[0].getBoundingClientRect(),
                promise;

            template.css({
                top: $el.offsetTop - 10 + 'px',
                left: $el.offsetLeft + rect.width + 20 + 'px'
            });

            element.find('label').on('mouseenter', function () {
                scope.$apply(function () {
                    if (promise) $animate.cancel(promise);
                    promise = $animate.enter(template, element.parent())
                })
            });

            element.find('label').on('mouseleave', function () {
                scope.$apply(function () {
                    if (promise) $animate.cancel(promise);
                    promise = $animate.leave(angular.element(document.querySelector('.tooltip')))
                })
            })
        }
    }
});