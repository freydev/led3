var $ = django.jQuery;

$(document).ready(function () {
    $('#map').click(function (e) {
        var offset = $(this).offset();
        var scrollTop = $(document).scrollTop();
        $('#ball').css({
            'left': e.clientX - offset.left - parseInt($('#ball').css('width')) / 2,
            'top': e.clientY - offset.top + scrollTop  - parseInt($('#ball').css('height')) / 2
        });

        $('#id_pos_x').val(Math.round(e.clientX - 50 - offset.left - parseInt($('#ball').css('width')) / 2));
        $('#id_pos_y').val(Math.round(e.clientY - 50 - offset.top + scrollTop - parseInt($('#ball').css('height')) / 2));
    })
});
