from django.db import models


class YmapCoord(models.CharField):
    def __init__(self, start_query, size_width=500, size_height=500, address_field='', *args, **kwargs):
        self.start_query, self.size_width, self.size_height, self.address_field = start_query, size_width, size_height, address_field
        super(YmapCoord, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(YmapCoord, self).deconstruct()
        kwargs['start_query'] = self.start_query
        kwargs['size_width'] = self.size_width
        kwargs['size_height'] = self.size_height
        kwargs['address_field'] = self.address_field
        return name, path, args, kwargs

    def formfield(self, **kwargs):
        if kwargs.has_key('widget'):
            kwargs['widget'] = kwargs['widget'](attrs={
                "data-start_query": self.start_query,
                "data-size_width": self.size_width,
                "data-size_height": self.size_height,
                "data-field": self.address_field,
            })
        return super(YmapCoord, self).formfield(**kwargs)