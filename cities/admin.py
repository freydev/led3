#! coding: UTF-8
from functools import partial

from django.db.models import Q
from django.contrib import admin
from django.contrib.admin import StackedInline
from django.template import Template, Context
from cities.models import City, MapBall
from django_ymap.admin import YmapAdmin
from itertools import chain

inline_factory = lambda model, **kwargs: type(
    model.__name__ + 'Admin',
    (StackedInline,),
    dict(model=model, extra=1, **kwargs)
)


class CityAdmin(YmapAdmin, admin.ModelAdmin):
    list_display = ('id', 'name', 'slug', '_mapball')
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name',)
    readonly_fields = ('_links',)
    ordering = ['id']

    def _mapball(self, obj):
        return ', '.join(obj.mapball_set.values_list('name', flat=True))

    def _links(self, obj):
        template = """
            {% for obj in balls %}
            <a href="{% url 'admin:cities_mapball_change' obj.id %}">{{ obj.name }}</a>
            {% endfor %}
        """

        return Template(template).render(Context({'balls': obj.mapball_set.all()}))

    _links.short_description = u'Связанные метки'
    _links.allow_tags = True


class MapBallAdmin(admin.ModelAdmin):
    class Media:
        js = ('js/mapball_admin.js',)

    list_display = ('id', 'name', 'pos_x', 'pos_y', 'size')
    search_fields = ('name',)
    filter_horizontal = ('cities',)
    readonly_fields = ('_get_world_map',)
    ordering = ['id']

    def get_form(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield, request=request, obj=obj)
        return super(MapBallAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name != "cities":
            kwargs.pop('obj', None)
        return super(MapBallAdmin, self).formfield_for_dbfield(db_field, **kwargs)


    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'cities':
            ball = kwargs.pop('obj', None)
            if ball:
                kwargs['queryset'] = City.objects.filter(Q(mapball__id=ball.id) | Q(mapball__isnull=True))
            else:
                kwargs['queryset'] = City.objects.filter(mapball__isnull=True)
        return super(MapBallAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def _get_world_map(self, obj):
        return '<div id="map" style="' \
               'float: left;' \
               'position: relative;' \
               'background-image: url(/static/img/rumap.png);' \
               'background-color: rgba(0,0,0,0.52);' \
               'background-repeat: no-repeat;' \
               'background-origin: content-box;' \
               'box-shadow: 0px 0px 10px 0px #D0CFCF inset;' \
               'padding: 50px;' \
               'cursor: pointer;' \
               'width: 923px;' \
               'height: 501px;">' \
               '<div id="ball" style="' \
               'border-radius: 50%;' \
               'background-color: #0496b0;' \
               'position: absolute;' \
               'top: {top}px;' \
               'left: {left}px;' \
               'height: {diameter}px;' \
               'width: {diameter}px;"></div>' \
               '</div>'.format(top=obj.pos_y + 50, left=obj.pos_x + 50, diameter=(obj.size) or 24)

    _get_world_map.short_description = u'Карта кликабельна'
    _get_world_map.allow_tags = True


admin.site.register(City, CityAdmin)
admin.site.register(MapBall, MapBallAdmin)
