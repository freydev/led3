from django import template
from django.template import Template, Context
from cities.models import City, MapBall
import json

register = template.Library()


@register.simple_tag
def get_cities_json():
    template = '''
        <script>window.CITIES = {{cities|safe}}</script>
    '''

    cities = list(City.objects.extra(select={'letter': 'substr(name, 1, 1)'}).values('name', 'letter', 'slug').order_by('name'))
    return Template(template).render(Context({
        'cities': json.dumps(cities)
    }))


@register.simple_tag
def get_balls_json():
    template = '''
        <script>window.BALLS = {{balls|safe}}</script>
    '''

    balls = [{
                 'pos_x': item.pos_x,
                 'pos_y': item.pos_y,
                 'size': item.size or item.cities.count() * 5,
                 'cities': [{'id':city.id, 'name': city.name, 'slug': city.slug, 'count': city.led_set.count() } for city in item.cities.all()]
             } for item in MapBall.objects.filter(cities__gt=0).distinct()]
    return Template(template).render(Context({
        'balls': json.dumps(balls)
    }))