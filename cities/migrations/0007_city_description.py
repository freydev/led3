# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0006_city_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='description',
            field=redactor.fields.RedactorField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
