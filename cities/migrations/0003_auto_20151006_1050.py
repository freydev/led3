# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0002_mapball'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='city',
            options={'verbose_name': '\u0413\u043e\u0440\u043e\u0434', 'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430'},
        ),
        migrations.AlterModelOptions(
            name='mapball',
            options={'verbose_name': '\u041c\u0435\u0442\u043a\u0443', 'verbose_name_plural': '\u041c\u0435\u0442\u043a\u0438 \u043d\u0430 \u043a\u0430\u0440\u0442\u0435 \u0420\u043e\u0441\u0441\u0438\u0438'},
        ),
        migrations.RemoveField(
            model_name='mapball',
            name='cities',
        ),
        migrations.AddField(
            model_name='city',
            name='map_ball',
            field=models.ForeignKey(verbose_name='\u041c\u0435\u0442\u043a\u0430 \u043d\u0430 \u043a\u0430\u0440\u0442\u0435', blank=True, to='cities.MapBall', null=True),
        ),
        migrations.AlterField(
            model_name='mapball',
            name='size',
            field=models.PositiveSmallIntegerField(default=0, help_text='0 - \u0432\u044b\u0447\u0438\u0441\u043b\u044f\u0435\u0442\u0441\u044f \u0438\u0437 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u0430 \u0441\u0432\u044f\u0437\u0430\u043d\u043d\u044b\u0445 \u0433\u043e\u0440\u043e\u0434\u043e\u0432', verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440 \u043a\u0440\u0443\u0433\u0430 \u0432 \u043f\u0438\u043a\u0441\u0435\u043b\u044f\u0445'),
        ),
    ]
