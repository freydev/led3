# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_ymap.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0004_auto_20151006_1104'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='center',
            field=django_ymap.fields.YmapCoord(default=None, size_width=980, max_length=200, blank=True, address_field=b'', size_height=500, null=True, verbose_name='\u0426\u0435\u043d\u0442\u0440', start_query='\u041c\u043e\u0441\u043a\u0432\u0430'),
        ),
    ]
