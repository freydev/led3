# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0003_auto_20151006_1050'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='city',
            name='map_ball',
        ),
        migrations.AddField(
            model_name='mapball',
            name='cities',
            field=models.ManyToManyField(to='cities.City', verbose_name='\u0421\u0432\u044f\u0437\u0430\u043d\u043d\u044b\u0435 \u0433\u043e\u0440\u043e\u0434\u0430'),
        ),
    ]
