#! coding: UTF-8
from django.db import models
from django.db.models import permalink
from django.template.defaultfilters import slugify
from django_ymap.fields import YmapCoord
from redactor.fields import RedactorField


class City(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Название')
    slug = models.SlugField(max_length=50, verbose_name=u'URL')
    description = RedactorField(blank=True, verbose_name=u'Описание')
    center = YmapCoord(max_length=200, default=None, blank=True, null=True, start_query=u'Москва', size_width=980,
                       size_height=500, verbose_name=u'Центр')

    @permalink
    def get_absolute_url(self):
        return 'city', self.id

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Города'


class MapBall(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Название зоны')
    size = models.PositiveSmallIntegerField(default=0, verbose_name=u'Размер круга в пикселях',
                                            help_text=u'0 - вычисляется из количества связанных городов')

    cities = models.ManyToManyField(City, verbose_name=u'Связанные города')
    pos_x = models.PositiveSmallIntegerField(default=0, verbose_name=u'Позиция по горизонтали')
    pos_y = models.PositiveSmallIntegerField(default=0, verbose_name=u'Позиция по вертикали')

    def get_size(self):
        return self.size

    class Meta:
        verbose_name = u'Метку'
        verbose_name_plural = u'Метки на карте России'

    def __unicode__(self):
        return self.name