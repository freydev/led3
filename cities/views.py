import json
from django.template import Context, Template
from django.views.generic import DetailView
from cities.models import City
from main.models import LedoutdoorSettings


class CityView(DetailView):
    model = City
    template_name = 'city.html'
    context_object_name = 'city'

    def get_context_data(self, **kwargs):
        context = super(CityView, self).get_context_data(**kwargs)
        settings = LedoutdoorSettings.objects.all()[0]

        from django.core import serializers
        data = json.loads(serializers.serialize("json", [self.object]))

        context['city_title'] = Template(settings.title_city).render(Context(data[0]['fields']))
        context['city_description'] = Template(settings.description_city).render(Context(data[0]['fields']))
        context['city_keywords'] = Template(settings.keywords_city).render(Context(data[0]['fields']))
        print(context)
        return context