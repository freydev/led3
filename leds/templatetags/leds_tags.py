import json
from django import template
from django.template import Template, Context
from leds.models import LedGroup

register = template.Library()


@register.simple_tag
def get_packages():
    template = '''
        <script>window.GROUPS = {{groups|safe}}</script>
    '''

    return Template(template).render(Context({
        'groups': json.dumps([{'id': pack.id, 'leds': list(pack.leds.values_list('id', flat=True))} for pack in LedGroup.objects.all()])
    }))
