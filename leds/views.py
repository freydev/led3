#coding: utf-8

import json
from django import http
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View
from leds.models import Led, LedGroup
from main.models import Order, Question, FastBid
from main.utils import ajax_required, sendmail


class JSONResponseMixin(object):
    def render_to_json(self, context):
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        return http.HttpResponse(content,
                                 content_type='application/json',
                                 **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context)


class JsonContent(JSONResponseMixin, View):
    def get_order(self):
        if self.request.GET.get('ids'):
            groups = LedGroup.objects.filter(leds__id__in=self.request.GET.get('ids').split(',')).distinct()
            leds_in_groups = []
            leds_in_request = [int(id) for id in self.request.GET.get('ids').split(',')]
            for group in groups:
                group_leds = list(group.leds.values_list('id', flat=True))
                print(group_leds, leds_in_request)
                print(list(set(group_leds) & set(leds_in_request)), group_leds)
                print(list(set(group_leds) & set(leds_in_request)) == group_leds)
                if list(set(group_leds) & set(leds_in_request)) == group_leds:
                    leds_in_groups = leds_in_groups + group_leds

            leds_in_groups = list(set(leds_in_groups))
            leds_not_in_groups = list(
                set(leds_in_request) - set(list(leds_in_groups)))
            leds = Led.objects.filter(id__in=leds_not_in_groups).values('id', 'name', 'address')
        else:
            leds = []
            groups = []

        context = {'leds': leds,
                   'groups': groups}
        return render(self.request, 'dialogs/order.html', context)

    def get_details(self):
        context = {'led': Led.objects.get(pk=self.request.GET.get('id'))}
        return render(self.request, 'dialogs/details.html', context)

    def get_fastbid(self):
        return render(self.request, 'dialogs/fastbid.html')

    def get(self, request, *args, **kwargs):
        methods = {
            'get_display': self.get_details,
            'get_order': self.get_order,
            'get_fastbid': self.get_fastbid
        }

        return methods.get(request.GET.get('method'))()

    def send_order(self):
        post = json.loads(self.request.body)
        order = Order.objects.create()
        order.company = post.get('company')
        order.person = post.get('person')
        order.email = post.get('email')
        order.phone = post.get('phone')
        order.comment = post.get('comment')

        for group in LedGroup.objects.filter(leds__id__in=post.get('leds')).distinct():
            order.groups.add(group)

        for led in Led.objects.filter(id__in=post.get('leds')):
            order.leds.add(led)

        order.save()
        sendmail(u'заявка', order)

        return HttpResponse({'status': 'OK'})

    def send_issue(self):
        post = json.loads(self.request.body)
        issue = Question.objects.create()
        issue.name = post.get('name')
        issue.phone = post.get('phone')
        issue.email = post.get('email')
        issue.issue = post.get('issue')
        issue.save()

        sendmail(u'вопрос', issue)
        return HttpResponse({'status': 'OK'})

    def send_bid(self):
        post = json.loads(self.request.body)
        bid = FastBid.objects.create()
        bid.text = post.get('bid')
        bid.save()

        sendmail(u'быстрая заявка', bid)
        return HttpResponse({'status': 'OK'})

    def set_success(self):
        success_type = self.request.POST.get('type', 'order')
        if success_type == 'order':
            obj = Order.objects.get(pk=self.request.POST.get('id'))
        if success_type == 'bid':
            obj = FastBid.objects.get(pk=self.request.POST.get('id'))
        if success_type == 'issue':
            obj = Question.objects.get(pk=self.request.POST.get('id'))

        obj.success = True
        obj.save()

        return HttpResponse({'status': 'OK'})

    def post(self, request, *args, **kwargs):
        method = {
            'send_order': self.send_order,
            'set_success': self.set_success,
            'send_issue': self.send_issue,
            'send_bid': self.send_bid
        }

        return method.get(request.GET.get('method'))()
