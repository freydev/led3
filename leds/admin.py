from django.contrib import admin
from django.contrib.admin import StackedInline
from django.contrib.admin.widgets import AdminFileWidget
from django.forms import ModelForm
from django.utils.safestring import mark_safe
from django_ymap.admin import YmapAdmin
from redactor.widgets import RedactorEditor
from sorl.thumbnail import get_thumbnail
from leds.models import LedImage, Led, LedGroup
from django.conf import settings

inline_factory = lambda model, **kwargs: type(
    model.__name__ + 'Admin',
    (StackedInline,),
    dict(model=model, extra=0, **kwargs)
)


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            t = get_thumbnail('%s/%s' % (settings.MEDIA_ROOT, value), '80x80')
            output.append('<img style="margin-right: 20px; float: left" src="{url}">'.format(url=t.url))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class ImageForm(ModelForm):
    class Meta:
        model = LedImage
        fields = '__all__'
        widgets = dict(image=AdminImageWidget)


class LedAdmin(YmapAdmin, admin.ModelAdmin):
    ordering = ['id']
    list_display = ('id', 'city', 'name' ,'address')
    list_filter = ('city',)
    inlines = [inline_factory(LedImage, form=ImageForm)]
    search_fields = ('name', 'address', 'city__name')
    raw_id_fields = ('city',)


class LedGroupAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ('id', 'name', 'city')
    raw_id_fields = ('city',)
    filter_horizontal = ('leds',)
    search_fields = ('name', 'city')


admin.site.register(Led, LedAdmin)
admin.site.register(LedGroup, LedGroupAdmin)
