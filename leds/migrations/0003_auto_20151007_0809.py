# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('leds', '0002_auto_20151007_0807'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledimage',
            name='led',
            field=models.ForeignKey(default='', to='leds.Led'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ledinfo',
            name='led',
            field=models.ForeignKey(default='', to='leds.Led'),
            preserve_default=False,
        ),
    ]
