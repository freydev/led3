# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0006_city_slug'),
        ('leds', '0006_led_center'),
    ]

    operations = [
        migrations.CreateModel(
            name='LedGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('city', models.ForeignKey(verbose_name='', to='cities.City')),
            ],
        ),
        migrations.AddField(
            model_name='led',
            name='text',
            field=redactor.fields.RedactorField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AddField(
            model_name='ledgroup',
            name='leds',
            field=models.ManyToManyField(to='leds.Led', verbose_name='\u0421\u0432\u044f\u0437\u0430\u043d\u043d\u044b\u0435 \u044d\u043a\u0440\u0430\u043d\u044b'),
        ),
    ]
