# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0006_city_slug'),
        ('leds', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LedImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'/uploads', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f')),
            ],
        ),
        migrations.CreateModel(
            name='LedInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='led',
            name='address',
            field=models.CharField(default='', max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='led',
            name='city',
            field=models.ForeignKey(default='', to='cities.City'),
            preserve_default=False,
        ),
    ]
