# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('leds', '0008_auto_20151011_1831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='led',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', to='cities.City'),
        ),
        migrations.AlterField(
            model_name='ledgroup',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', to='cities.City'),
        ),
    ]
