# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('leds', '0007_auto_20151010_1301'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ledinfo',
            name='led',
        ),
        migrations.AlterModelOptions(
            name='led',
            options={'verbose_name': '\u042d\u043a\u0440\u0430\u043d', 'verbose_name_plural': '\u042d\u043a\u0440\u0430\u043d\u044b'},
        ),
        migrations.AlterModelOptions(
            name='ledgroup',
            options={'verbose_name': '\u041f\u0430\u043a\u0435\u0442', 'verbose_name_plural': '\u041f\u0430\u043a\u0435\u0442\u044b \u044d\u043a\u0440\u0430\u043d\u043e\u0432'},
        ),
        migrations.DeleteModel(
            name='LedInfo',
        ),
    ]
