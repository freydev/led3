# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('leds', '0003_auto_20151007_0809'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ledimage',
            name='image',
            field=models.ImageField(upload_to=b'uploads', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f'),
        ),
    ]
