# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('leds', '0004_auto_20151007_0815'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='led',
            options={'verbose_name': '\u042d\u043a\u0440\u0430\u043d\u044b', 'verbose_name_plural': '\u042d\u043a\u0440\u0430\u043d'},
        ),
        migrations.AlterModelOptions(
            name='ledimage',
            options={'verbose_name': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f \u044d\u043a\u0440\u0430\u043d\u0430', 'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438'},
        ),
        migrations.AlterModelOptions(
            name='ledinfo',
            options={'verbose_name': '\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e\u0431 \u044d\u043a\u0440\u0430\u043d\u0435', 'verbose_name_plural': '\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f'},
        ),
        migrations.AlterField(
            model_name='ledinfo',
            name='text',
            field=redactor.fields.RedactorField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435'),
        ),
    ]
