#! coding: UTF-8
from django.db import models
from django_ymap.fields import YmapCoord
from redactor.fields import RedactorField


class Led(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Название')
    city = models.ForeignKey('cities.City', verbose_name=u'Город')
    address = models.CharField(max_length=255, verbose_name=u'Адрес')
    text = RedactorField(blank=True, verbose_name=u'Описание')

    center = YmapCoord(max_length=200, default=None, blank=True, null=True, start_query=u'Москва', size_width=980,
                       size_height=500, verbose_name=u'Центр')


    class Meta:
        verbose_name = u'Экран'
        verbose_name_plural = u'Экраны'

    def __unicode__(self):
        return self.name

class LedImage(models.Model):
    led = models.ForeignKey('Led')
    image = models.ImageField(upload_to='uploads', verbose_name=u'Фотография')

    class Meta:
        verbose_name = u'Фотография экрана'
        verbose_name_plural = u'Фотографии'

    def __unicode__(self):
        return u''


class LedGroup(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Название')
    leds = models.ManyToManyField('Led', verbose_name=u'Связанные экраны')
    city = models.ForeignKey('cities.City', verbose_name=u'Город')

    def led_array(self):
        return list(self.leds.values_list('id', flat=True))

    class Meta:
        verbose_name = u'Пакет'
        verbose_name_plural = u'Пакеты экранов'

    def __unicode__(self):
        return self.name